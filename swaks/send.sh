#!/bin/sh
swaks \
    -t ${__TARGET__} \
    -f ${__FROM__} \
    -s relay.express-mailing.com \
    -p 465 \
    -tlsc \
    -au ${__USERNAME__} \
    -ap ${__PASSWORD__} \
    --header "Subject: Bonjour ##PRENOM##" \
    --header "x-em-notify-url: ${__NOTIFY_URL__}"
    --header "x-em-external-id: ${__EXTERNAL_ID__}"
    --attach-type text/html --attach-body ../body.html