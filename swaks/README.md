# smtp-relay-client

Exemple d'utilisation du relais SMTP depuis un terminal linux avec swaks.

## Pour commencer

Installer swaks sur votre système : https://jetmore.org/john/code/swaks/installation.html

Editer le fichier send.sh et modifier les valeur des variables

 - ${__USERNAME__}          => identifiant de votre compte
 - ${__PASSWORD__}          => mot de passe de votre compte
 - ${__FROM__}              => Expéditeur du message
 - ${__TARGET__}            => Destinataire du message
 - ${__NOTIFY_URL__}        => Url de notification du message
 - ${__EXTERNAL_ID__}       => ID de votre message

Executez le script :

```sh
./send.sh
```

## Voir aussi

 - Swaks : https://github.com/jetmore/swaks