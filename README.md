# smtp-relay-client

Exemple d'utilisation du relais SMTP depuis un code externe.

## Connexion à nos serveurs

Vous pouvez vous connecter à nos serveurs SMTP via les URL suivantes :

- **relay.express-mailing.com** : relais simple sans tracking et sans statistiques.
- **relay-stats.express-mailing.com** : relais avec tracking et statistiques.
- **relay-transac.express-mailing.com** : relais transactionnel sans tracking et sans statistiques.

Vous pouvez utiliser les ports suivants :

- 465 : sécurisé - **recommandé**.
- 25 : non sécurisé.
- 587 : non sécurisé par défaut - nécessite StartTLS pour activer le chiffrement.

## Améliorer les statistiques

Pour mieux profiter des statistiques, vous pouvez utiliser les headers suivants :

| **Entête de l'email** | **Valeur**                                           |
|-----------------------|------------------------------------------------------|
| `x-em-list`           | (Optionnel, string) Nom de la liste associée à cet envoi |
| `x-em-campaign`       | (Optionnel, string) Nom de la campagne associée à cet envoi  |

## Priorité de l'envoi

Pour délivrer les messages cruciaux au plus vite, vous pouvez utiliser l'entête suivant :

| **Entête de l'email** | **Valeur**                                         |
|-----------------------|----------------------------------------------------|
| `x-em-priority`       | (Optionnel, 1) Si cet entête est positionné à 1, alors l'envoi est prioritaire |

## Envoi transactionnel

Vous pouvez utiliser le header suivant pour envoyer vos messages en mode transactionnel (ou utiliser le SMTP spécifique relay-transac.express-mailing.com).

Les mails transactionnels sont automatiquement prioritaires et passent outre la mise en liste noire et le désabonnement. Vous ne devez jamais utiliser ce mode pour des envois prospectifs ou promotionnels. En cas de doute, demandez-nous conseil ou n'utilisez pas ce mode d'envoi.

| **Entête de l'email** | **Valeur**                                                                                   |
|-----------------------|----------------------------------------------------------------------------------------------|
| `x-em-mode`           | (Optionnel, 'transac') Si cet entête est positionné à 'transac', alors l'envoi est transactionnel |

## Obtenir les événements des messages

Il vous est possible de recevoir des notifications pour suivre vos envois. Il vous faudra préalablement préparer un serveur pour recevoir nos notifications sous forme de POST HTTP.

### Activer les notifications d'un message

Pour activer les notifications, vous devrez indiquer les entêtes emails suivants :

| **Entête de l'email** | **Valeur**                               |
|-----------------------|------------------------------------------|
| `x-em-notify-url`     | URL de la page à notifier                |
| `x-em-external-id`    | (Optionnel) Identifiant de votre message |

### Recevoir les notifications d'un message

Les notifications sont effectuées sous la forme d'un appel HTTP POST, le contenu de ce post est un document JSON structuré comme suit :

```json
{
    "date": "dateIsoString",
    "is_relay": "boolean",
    "account": "string | null",
    "message": "string | null",
    "list": "string | null",
    "title": "string | null",
    "from": "string | null",
    "to": "string | null",
    "id": "string | null",
    "event": "mail-prepared | mail-canceled | mail-bounced | mail-delivered | mail-opened | mail-clicked",
    "bounce_type": "ABSENCE | BLACKLIST | CHANGEMAIL | CONTENU | GREYLIST | INCONNU | QUOTA | RELAY | SERVER | SPAM | SUCCESS | TECHNIQUE | USER | WORD_TITLE | null",
    "expression": "string | null",
    "extract": "string | null",
    "ip": "string | null",
    "ua": {
        "isMobile": "boolean",
        "isDesktop": "boolean",
        "isBot": "boolean",
        "isSmartTV": "boolean",
        "isTablet": "boolean",
        "deviceFamily": "unknown | bot | desktop | tablet | tv | mobile",
        "browser": "string",
        "version": "string",
        "os": "string",
        "platform": "string",
        "source": "string"
    }
}
```

#### bounce_types

**QUOTA :** Erreur de quota. La boîte de réception du destinataire est pleine.

**GREYLIST :** Email retardé. La boîte de réception du destinataire est protégée par un système de liste grise (GreyList).

**ABSENCE :** Message indiquant que l'adresse email du destinataire est valide, mais que celui-ci prendra connaissance du courriel ultérieurement (en raison d'absence pour voyage, congés, maladie, congé maternité, etc.).

**CHANGEMAIL :** Message indiquant que l'adresse email du destinataire est valide, mais n'est plus correcte. Exemple : "Je ne suis plus à cette adresse, veuillez me contacter à l'adresse suivante ..." ou "Je ne fais plus partie de cette entreprise, veuillez contacter mon remplaçant à l'adresse suivante ..."

**USER :** Utilisateur inconnu. L'adresse email du destinataire ne semble pas ou plus exister.

**SERVER :** Serveur invalide ou occupé. Le serveur de messagerie du destinataire est invalide ou temporairement inaccessible.

**RELAY :** Erreur de relay. Le serveur de messagerie impose des vérifications strictes de concordance entre l'adresse email de l'expéditeur et l'IP du serveur d'envoi.

**TECHNIQUE :** Erreur de route ou de DNS. Le serveur de messagerie du destinataire semble mal configuré ou refuse les emails provenant de notre plate-forme, souvent pour limiter la réception de publicités.

**SPAM :** Blocage par un anti-spam. Le serveur de messagerie du destinataire a bloqué l'email, généralement en raison du contenu ou de l'objet du message.

**BLACKLIST :** Blocage par un anti-spam. Le serveur de messagerie du destinataire a bloqué l'email, souvent lié au domaine ou à l'IP d'émission.

**CONTENU / WORD\_TITLE :** Blocage par un anti-spam. Le serveur de messagerie du destinataire a bloqué l'email, généralement en raison du contenu ou de l'objet du message.

**INCONNU :** Aucune correspondance trouvée par notre analyseur.

**SUCCESS :** L'email a été correctement délivré.

 
## Utilisation des listes de diffusion

Pour envoyer un message vers une liste gérée sur la plateforme my.express-mailing.com, vous devrez récupérer son code rouge et envoyer votre mail à une adresse de type CODE_ROUGE@list.express-mailing.com.

Lors d'un envoi vers une liste de diffusion, vous DEVEZ proposer le désabonnement à vos destinataires en utilisant l'url spéciale de désabonnement :

| **Mot clé**  | **Valeur**           |
|--------------|----------------------|
| `##DESAB##`  | URL de désabonnement |

Exemple :

```html
<a href="##DESAB##">Désabonnement</a>
```

## Personnalisation des messages

Vous pouvez personnaliser le contenu de vos messages avec les données enregistrées sur la plateforme avec des mots clés spécifiques :

| **Mot clé**            | **Valeur**                    |
|------------------------|-------------------------------|
| `##EMAIL##`            | Adresse email de l'abonné     |
| `##NOM##`              | Nom de l'abonné               |
| `##PRENOM##`           | Prénom de l'abonné            |
| `##CIV##`              | Civilité courte (M., Mme)     |
| `##CIVILITE##`         | Civilité longue (Monsieur, Madame) |
| `##CHER##`             | Cher/Chère (selon civilité)   |
| `##E##`                | 'e' final des mots féminins   |
| `##SOCIETE##`          | Société de l'abonné           |
| `##CODE##`             | Code Client                   |
| `##FONCTION##`         | Fonction / Métier             |
| `##SIRET##`            | N° de Siret                   |
| `##NAF##`              | Code NAF/APE                  |
| `##ACTIVITE##`         | Activité de l'entreprise      |
| `##ADRESSE1##`         | Adresse postale 1             |
| `##ADRESSE2##`         | Adresse postale 2             |
| `##ADRESSE3##`         | Adresse postale 3             |
| `##CP##`               | Code postal                   |
| `##VILLE##`            | Ville                         |
| `##REGION##`           | Région                        |
| `##PAYS##`             | Pays                          |
| `##TEL##`              | Numéro de téléphone           |
| `##FAX##`              | Numéro de fax                 |
| `##GSM##`              | Téléphone portable GSM        |
| `##WEB##`              | Site Internet                 |
| `##LIBRE1##`           | Champ Libre 1                 |
| `##LIBRE2##`           | Champ Libre 2                 |
| `##LIBRE3##`           | Champ Libre 3                 |
| `##LIBRE4##`           | Champ Libre 4                 |
| `##LIBRE5##`           | Champ Libre 5                 |
| `##NOM_EXPEDITEUR##`   | Libellé de l'expéditeur       |
| `##EMAIL_EXPEDITEUR##` | Email de l'expéditeur         |

## Exemples

Vous trouverez, dans les différents dossiers, des exemples d'utilisation de notre relais SMTP dans différents langages.

- PHP avec PHPMailer : [./php](./php)
- SH avec SWAKS : [./swaks](./swaks)

Nous pouvons ajouter un langage sur demande à support@express-mailing.zendesk.com.