<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

$__USERNAME__ = $_ENV["__USERNAME__"];
$__PASSWORD__ = $_ENV["__PASSWORD__"];
$__FROM__ = $_ENV["__FROM__"];
$__TARGET__ = $_ENV["__TARGET__"];
$__NOTIFY_URL__ = $_ENV["__NOTIFY_URL__"];
$__EXTERNAL_ID__ = $_ENV["__EXTERNAL_ID__"];

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'relay.express-mailing.com';            //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $__USERNAME__;                           //SMTP username
    $mail->Password   = $__PASSWORD__;                           //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom($__FROM__);
    $mail->addAddress($__TARGET__);                                 //Add a recipient

    //Notifications
    $mail->addCustomHeader('x-em-notify-url', $__NOTIFY_URL__);     // URL de notification
    $mail->addCustomHeader('x-em-external-id', $__EXTERNAL_ID__);    //ID de l'email

    //Content
    $body = file_get_contents('./files/body.html');
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->CharSet = 'UTF-8';
    $mail->Subject = 'Bonjour ##PRENOM##';
    $mail->Body    = $body;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}