# smtp-relay-client

Exemple d'utilisation du relais SMTP depuis un code PHP avec l'extention PHPMailer.

## Pour commencer

Initialiser les dépandances du projet :

```sh
composer install
```

Editer le fichier ./send.php et initialiser les valeurs des variables :

 - $__USERNAME__    => identifiant de votre compte
 - $__PASSWORD__    => mot de passe de votre compte
 - $__FROM__        => Expéditeur du message
 - $__TARGET__      => Destinataire du message
 - $__NOTIFY_URL__  => Url de notification du message
 - $__EXTERNAL_ID__ => ID de votre message

Exécuter le script :

```sh
php ./send.php
```

## Voir aussi

 - PHPMailer : https://github.com/PHPMailer/PHPMailer
 - Composer : https://getcomposer.org/doc/00-intro.md